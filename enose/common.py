import time
import math


class common():

    def __init__(self, mcpadc, sampling):
        self.adc = mcpadc
        self.sampling = sampling

    def mqresistance1(self, meas, rl, precision):
        raw_adc = meas['a']
        rs = float(rl*(precision-raw_adc)/float(raw_adc))
        return rs

    def mqresistance2(self, meas, rl, v, precision):
        raw_adc = meas['a']
        vrl = float((raw_adc * v) / precision)
        rs = float(((v/vrl)-1) * rl)
        return rs

    def mqresistance3(self, meas, rl, v):
        vrl = float(meas['v'])
        rs = float(v - vrl)/float(vrl / rl)
        return rs

    # Using 16 bits floating point for MCP3008 instead of 10 bits
    def mqresistance(self, meas, rl, v=3.3, precision=65535):
        rs1 = self.mqresistance1(meas, rl, precision)
        rs2 = self.mqresistance2(meas, rl, v, precision)
        rs3 = self.mqresistance3(meas, rl, v)
        # print("rs1: " + str(rs1))
        # print("rs2: " + str(rs2))
        # print("rs3: " + str(rs3))
        rs = float(rs1 + rs2 + rs3) / 3
        return rs

    def mqcal(self, mq_pin, ro_clean_air, rl):
        val = 0.0
        for i in range(self.sampling['cal_sampling_times']):
            val += self.mqresistance(self.adc.read(mq_pin), rl)
            time.sleep(self.sampling['cal_sampling_interval']/1000.0)
        val = float(val/self.sampling['cal_sampling_times']) / ro_clean_air
        return val

    def mqadc(self, mq_pin, rl):
        rs = 0.0
        for i in range(self.sampling['measurement_sampling_times']):
            rs += self.mqresistance(self.adc.read(mq_pin), rl)
            time.sleep(self.sampling['measurement_sampling_interval']/1000.0)
        rs = rs/self.sampling['measurement_sampling_times']
        return rs

    def mqppm(self, rs_ro_ratio, points):
        yt = float(points["yt"])
        xt = float(points["xt"])
        yb = float(points["yb"])
        xb = float(points["xb"])
        m = float(math.log10(yt) - math.log10(yb)) / \
            float(math.log10(xt) - math.log10(xb))
        logppm = ((math.log(rs_ro_ratio) - yb)/m) - yt
        ppm = math.pow(10, logppm)
        return ppm
