import csv
import pandas as pd
from datetime import datetime
import os
import json
import base64
import time
import seaborn as sns


class data():

    def __init__(self, directory, iscsv):
        now = datetime.now()
        self.data = directory
        if not os.path.exists(self.data):
            os.makedirs(self.data)
        if(iscsv):
            self.today = now.strftime("%b_%d_%Y__%H_%M_%S") + ".csv"

    def read(self, filename):
        jobj = {}
        if os.path.exists(os.path.join(self.data, filename)):
            with open(os.path.join(self.data, filename), 'r') as f:
                jobj = json.load(f)
                f.close()
        return jobj

    def write(self, filename, content, mode="w"):
        with open(os.path.join(self.data, filename), mode) as f:
            f.write(content)
            f.close()

    def tocsv(self, content=[], timestamp=False):
        with open(os.path.join(self.data, self.today), 'a') as f:
            writer = csv.writer(f)
            if (timestamp == True):
                now = datetime.now()
                content.insert(0, now.strftime("%B %d, %Y") +
                               " " + now.strftime("%H:%M:%S"))
            writer.writerow(content)
            f.close()

    def csvtojson(self, csvfile, jsonfile, jsonfilename, normalizestep, convert2json=False):
        if(convert2json):
            df = pd.read_csv(csvfile)
            df.to_json(jsonfile, indent=4)
            if os.path.exists(csvfile):
                os.remove(csvfile)
        else:
            df = pd.read_csv(csvfile)
            obj = json.loads(df.to_json())
            jobj = {}
            for key in obj:
                value = obj[key]
                if not (key == "TEMPERATURE" or key == "HUMIDITY"):
                    if not jobj.__contains__(key):
                        jobj[key] = []
                    for skey in value:
                        jobj[key].append(value[skey])
            minmax = self.minmax(jobj, normalizestep)
            normalize = self.normalize(minmax)
            self.write(jsonfilename, json.dumps(
                normalize, indent=4, sort_keys=False))
            self.toheatmap(normalize, jsonfile, jsonfilename)

    def tojson(self, normalizestep):
        if os.path.exists(os.path.join(self.data, self.today)):
            print("WRITING JSON OBJECT")
            jsonfile = self.today[0:len(self.today)-4] + ".json"
            self.csvtojson(os.path.join(self.data, self.today),
                           os.path.join(self.data, jsonfile), jsonfile, normalizestep)

    def normalize(self, minmax):
        mdata = minmax["jobj"]
        rdata = minmax["jrange"]
        jnorm = {}
        for key in mdata:
            if not jnorm.__contains__(key):
                jnorm[key] = []
            # formula for normalization
            if (key != "DATETIME"):
                qty = len(mdata[key])
                for i in range(0, qty):
                    top = float(mdata[key][i]) - float(rdata[key]["min"])
                    bottom = float(rdata[key]["max"]) - \
                        float(rdata[key]["min"])
                    if (bottom > 0):
                        xnorm = top / bottom
                    else:
                        xnorm = 0.0
                    jnorm[key].append(xnorm)
            else:
                jnorm[key] = mdata[key]

        return jnorm

    def minmax(self, obj, normalizestep):
        jobj = {}
        jrange = {}
        mobj = {}
        for key in obj:
            if not jobj.__contains__(key):
                jobj[key] = []
                if (key != "DATETIME"):
                    jrange[key] = {}
            qty = len(obj[key])
            for i in range(0, qty, normalizestep):
                jobj[key].append(obj[key][i])

            if (key != "DATETIME"):
                jrange[key]["max"] = max(jobj[key])
                jrange[key]["min"] = min(jobj[key])

            mobj["jobj"] = jobj
            mobj["jrange"] = jrange

        return mobj

    def toheatmap(self, obj, fullpath, filename, isreverse=True):
        print('SAVING HEATMAP')
        fullpath = fullpath[0:len(fullpath)-5] + ".png"
        cols = []
        rows = []
        index = []
        for key in obj:
            if key != "DATETIME":
                cols.append(key)
            else:
                index = obj[key]

        qty = len(obj[cols[0]])
        for i in range(0, qty):
            r = []
            for k in cols:
                r.append(obj[k][i])
            rows.append(r)

        cmap = "gray"
        if(isreverse):
            index.reverse()
            cmap = "gray_r"

        df = pd.DataFrame(rows, columns=cols)
        sns.set(rc={'figure.figsize': (20, 10)})
        map = sns.heatmap(df, cmap=cmap,  yticklabels=index[0:qty-1])
        fig = map.get_figure()
        fig.savefig(fullpath, dpi=400)
        self.tobase64(fullpath, filename)

    def tobase64(self, fullpath, filename):
        print('SAVING BASE64 STRING')
        with open(fullpath, "rb") as img:
            plain = base64.b64encode(img.read())
            filename = filename[0:len(filename)-5] + ".base64"
            self.write(filename, plain.decode('utf-8'))
