from mq3 import mq3
from mq6 import mq6
from mq8 import mq8
from mq135 import mq135
from dht import dht
from decimal import Decimal
from data import data

import sys
import time
import os
import json

from signal import pause
from mcp3008 import mcp3008
from gpiozero import Button, LED


class activity():

    def __init__(self, lcdout):
        self.normalize_step = 1
        self.mcpadc = mcp3008()
        self.lcdout = lcdout
        self.button = Button(21,  pull_up=True, hold_time=1, hold_repeat=True)
        self.fan = LED(18)

    def write(self, info):
        # sys.stdout.write(info)
        print(info)

    def task(self, docal):
        try:
            counter = 0
            os.system('clear')

            print("Press CTRL+C to abort...")
            dht_ = dht()

            store = data("./data", True)
            config = data("./config", False)
            conf = config.read("config.json")

            self.lcdout.showtext(["WARMING UP"])
            self.fan.on()
            time.sleep(30)
            self.fan.off()

            dhta = dht_.measure(conf["dht"]["pin"])
            print("TEMPERATURE: {0:0.1f}*C  HUMIDITY: {1:0.1f}% ".format(
                dhta["TEMPERATURE"], dhta["HUMIDITY"]))

            if (docal):
                self.lcdout.showtext(["CALIBRATION STARTED"])

            mq_3 = mq3(self.mcpadc, conf["sampling"], conf["curves"]["mq3"])
            mq_6 = mq6(self.mcpadc, conf["sampling"], conf["curves"]["mq6"])
            mq_8 = mq8(self.mcpadc, conf["sampling"], conf["curves"]["mq8"])
            mq_135 = mq135(
                self.mcpadc,  conf["sampling"], conf["curves"]["mq135"])

            if docal == False:
                mq_3.setup(docal, self.lcdout, conf["ro"]['mq3'])
                time.sleep(1)
                mq_6.setup(docal, self.lcdout, conf["ro"]['mq6'])
                time.sleep(1)
                mq_8.setup(docal, self.lcdout, conf["ro"]['mq8'])
                time.sleep(1)
                mq_135.setup(docal, self.lcdout, conf["ro"]['mq135'])
                time.sleep(1)
            else:
                mq_3.setup(docal, self.lcdout,)
                mq_6.setup(docal, self.lcdout,)
                mq_8.setup(docal, self.lcdout,)
                mq_135.setup(docal, self.lcdout,)

                conf["ro"]["mq3"] = mq_3.getro()
                conf["ro"]["mq6"] = mq_6.getro()
                conf["ro"]["mq8"] = mq_8.getro()
                conf["ro"]["mq135"] = mq_135.getro()

                config.write("config.json",  json.dumps(
                    conf, indent=4, sort_keys=False), "w")

            if (docal):
                self.lcdout.showtext(["CALIBRATION DONE"])

            store.tocsv(
                [
                    "DATETIME",
                    "TEMPERATURE",
                    "HUMIDITY",
                    "ALCOHOL",
                    "LPG",
                    "CH4",
                    "HYDROGEN",
                    "NH3",
                    "ACETONE",
                    "CO",
                    "C02"
                ])

            time.sleep(3)
            self.lcdout.showtext(["TESTING STARTED"])
            time.sleep(1)

            max = conf["sampling"]["test_sampling_count"]
            normalize_step = conf["sampling"]["normalize_step"]

            while True:
                counter += 1
                if (self.button.is_pressed or (counter > max)):
                    self.lcdout.showtext(["...EXITING..."])
                    break

                self.lcdout.showtext(
                    ["TESTING STARTED", "LOOP: [ " + str(counter) + " / " + str(max) + " ]"])
                mq3a = mq_3.getmeasurement()
                mq6a = mq_6.getmeasurement()
                mq8a = mq_8.getmeasurement()
                mq135a = mq_135.getmeasurement()
                dhta = dht_.measure(conf["dht"]["pin"])

                self.write("\r")
                self.write("\033[K")
                self.write("COUNTER: {v}/{m} -> ".format(v=counter, m=max))
                self.write("TEMPERATURE: {0:0.1f}*C  HUMIDITY: {1:0.1f}%, ".format(
                    dhta["TEMPERATURE"], dhta["HUMIDITY"]))
                self.write("ALCOHOL: %g ppm" % (mq3a["ALCOHOL"]))
                self.write(", ")
                self.write("LPG: %g ppm, CH4: %g ppm" %
                           (mq6a["LPG"], mq6a["CH4"]))
                self.write(", ")
                self.write("H2: %g ppm" % (mq8a["H2"]))
                self.write(", ")
                self.write("NH3: %g ppm, ACETONE: %g ppm, C0: %g ppm, C02: %g ppm" %
                           (mq135a["NH3"], mq135a["ACETONE"], mq135a["CO"], mq135a["CO2"]))

                store.tocsv(
                    [
                        Decimal(dhta["TEMPERATURE"]),
                        Decimal(dhta["HUMIDITY"]),
                        Decimal(mq3a["ALCOHOL"]),
                        Decimal(mq6a["LPG"]),
                        Decimal(mq6a["CH4"]),
                        Decimal(mq8a["H2"]),
                        Decimal(mq135a["NH3"]),
                        Decimal(mq135a["ACETONE"]),
                        Decimal(mq135a["CO"]),
                        Decimal(mq135a["CO2"])
                    ], True
                )

                sys.stdout.flush()
                time.sleep(0.1)

        except KeyboardInterrupt:
            print("\nAborted by user.")
        finally:
            try:
                print("\n")
                store.tojson(normalize_step)
            except FileNotFoundError or ValueError:
                pass
            finally:
                self.lcdout.showtext(["TESTING COMPLETED"])
                self.exec(False)

    def exec(self, watch_held=True):
        time.sleep(5)
        self.lcdout.showtextno_date_ip(["WELCOME e-NOSE", "[ PRESS BUTTON ]",
                                        "> 5s = NO CAL",
                                        "> 3s = WITH CAL"])
        if(watch_held):
            self.button.when_held = self.docalibration

    def docalibration(self):
        print(str(self.button.active_time))
        if(int(self.button.active_time) >= 5 and self.button.is_pressed):
            self.lcdout.showtext(["CALIBRATION", "DISABLED"])
            time.sleep(2)
            self.task(False)
        elif(int(self.button.active_time) < 5):
            time.sleep(2)
            if(self.button.is_pressed == False):
                self.lcdout.showtext(["CALIBRATION", "ENABLED"])
                time.sleep(2)
                self.task(True)
