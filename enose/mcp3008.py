import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn


class mcp3008():
    def __init__(self):
        spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)
        cs = digitalio.DigitalInOut(board.D8)
        self.mcp = MCP.MCP3008(spi, cs)

    def read(self, channel):
        reading = AnalogIn(self.mcp, channel)
        value = {}
        value['v'] = reading.voltage
        value['a'] = reading.value  # >> 6 #For 10 bits precision uncomment it
        return value
