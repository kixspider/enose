import subprocess
import time

from datetime import datetime
from i2c_lcd import I2cLcd
from lcd_api import LcdApi


class lcd():

    rows = 4
    cols = 20
    addr = 0x27  # 39

    def __init__(self):
        self.lcd_display = I2cLcd(1, self.addr, self.rows, self.cols)
        self.lcd_display.clear()

    def showtext(self, content, y=0):
        now = datetime.now()
        self.lcd_display.clear()
        self.display(content, y)
        self.display([now.strftime("%m/%d/%Y") +
                     " " + now.strftime("%H:%M:%S")], 2)
        self.display(self.systemip(["hostname", "-I"]), 3)

    def showtextno_date_ip(self, content, y=0):
        self.lcd_display.clear()
        self.display(content, y)

    def display(self, content, y=0):
        print(content)
        location = y
        for text in content:
            if (len(text) > self.cols):
                text = text[0:self.cols-1]
            else:
                point = int((self.cols - len(text))/2)
                self.lcd_display.move_to(point, location)
                if (y == 0):
                    location += 1
            self.lcd_display.putstr(text)

    def systemip(self, command):
        ip = subprocess.check_output(command).split()
        if (ip is None or len(ip)== 0):
            return ""
        else:
            if(type(ip[0]) is str):
                return [ip[0]]
            return [ip[0].decode('utf-8')]
