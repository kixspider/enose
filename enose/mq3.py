from common import common

# https://www.sparkfun.com/datasheets/Sensors/MQ-3.pdf


class mq3():

    def __init__(self, mcadc,sampling, conf):
        self.common = common(mcadc,sampling)
        self.ALC = conf["ALC"]

        self.RL = conf["RL"]
        self.RO = conf["RO"]
        self.PIN = conf["PIN"]

    def setup(self, docal, lcdout, calRo=0):
        if(docal):
            self.RO = self.common.mqcal(
                self.PIN, self.RO, self.RL)
        elif calRo > 0:
            self.RO = calRo

        lcdout.showtext(["CALIBRATION","MQ3: Ro={:.2f}K".format(self.RO)])

    def getmeasurement(self):
        mq = {}
        read = self.common.mqadc(self.PIN, self.RL)
        mq['ALCOHOL'] = self.common.mqppm(read/self.RO, self.ALC)
        return mq

    def getro(self):
        return self.RO
