from common import common

# https://dlnmh9ip6v2uc.cloudfront.net/datasheets/Sensors/Biometric/MQ-8.pdf


class mq8():

    def __init__(self, mcadc,sampling,  conf):
        self.common = common(mcadc,sampling)
        self.H2 = conf["H2"]

        self.RL = conf["RL"]
        self.RO = conf["RO"]
        self.PIN = conf["PIN"]

    def setup(self, docal, lcdout, calRo=0):
        if(docal):
            self.RO = self.common.mqcal(
                self.PIN, self.RO, self.RL)
        elif calRo > 0:
            self.RO = calRo

        lcdout.showtext(["CALIBRATION","MQ8: Ro={:.2f}K".format(self.RO)])

    def getmeasurement(self):
        mq = {}
        read = self.common.mqadc(self.PIN, self.RL)
        mq['H2'] = self.common.mqppm(
            read/self.RO, self.H2)
        return mq

    def getro(self):
        return self.RO
