from common import common
# https://www.electronicoscaldas.com/datasheet/MQ-135_Hanwei.pdf


class mq135():

    def __init__(self, mcadc,sampling, conf):
        self.common = common(mcadc,sampling)
        self.NH3 = conf["NH3"]
        self.CO = conf["CO"]
        self.CO2 = conf["CO2"]
        self.ACETONE = conf["ACETONE"]

        self.RL = conf["RL"]
        self.RO = conf["RO"]
        self.PIN = conf["PIN"]

    def setup(self, docal, lcdout, calRo=0):
        if(docal):
            self.RO = self.common.mqcal(
                self.PIN, self.RO, self.RL)
        elif calRo > 0:
            self.RO = calRo

        lcdout.showtext(["CALIBRATION","MQ135: Ro={:.2f}K".format(self.RO)])

    def getmeasurement(self):
        mq = {}
        read = self.common.mqadc(self.PIN, self.RL)

        mq['NH3'] = self.common.mqppm(
            read/self.RO, self.NH3)

        mq['CO'] = self.common.mqppm(
            read/self.RO, self.CO)

        mq['CO2'] = self.common.mqppm(
            read/self.RO, self.CO2)

        mq['ACETONE'] = self.common.mqppm(
            read/self.RO, self.ACETONE)

        return mq

    def getro(self):
        return self.RO
