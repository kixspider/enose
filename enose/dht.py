
import Adafruit_DHT


class dht():

    def __init__(self):
        pass

    def measure(self, pin):
        dht = {}
        humidity, temperature = Adafruit_DHT.read_retry(
            Adafruit_DHT.DHT11, pin)
        if (humidity is not None) and (temperature is not None):
            dht['HUMIDITY'] = humidity
            dht['TEMPERATURE'] = temperature
        else:
            dht['HUMIDITY'] = 999
            dht['TEMPERATURE'] = 999
        return dht
