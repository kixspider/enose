from common import common

# https://www.sparkfun.com/datasheets/Sensors/Biometric/MQ-6.pdf


class mq6():

    def __init__(self, mcadc,sampling, conf):
        self.common = common(mcadc,sampling)
        self.LPG = conf["LPG"]
        self.CH4 = conf["CH4"]

        self.RL = conf["RL"]
        self.RO = conf["RO"]
        self.PIN = conf["PIN"]

    def setup(self, docal, lcdout, calRo=0):
        if(docal):
            self.RO = self.common.mqcal(
                self.PIN, self.RO, self.RL)
        elif calRo > 0:
            self.RO = calRo

        lcdout.showtext(["CALIBRATION","MQ6: Ro={:.2f}K".format(self.RO)])

    def getmeasurement(self):
        mq = {}
        read = self.common.mqadc(self.PIN, self.RL)

        mq['LPG'] = self.common.mqppm(
            read/self.RO, self.LPG)

        mq['CH4'] = self.common.mqppm(
            read/self.RO, self.CH4)

        return mq

    def getro(self):
        return self.RO
