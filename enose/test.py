from lcd import lcd
from signal import pause
from activity import activity

try:
    lcdout = lcd()
    lcdout.showtext(["WELCOME e-NOSE", "INITIALIZATION"])
    start = activity(lcdout)
    start.exec()
    pause()
except KeyboardInterrupt:
    pass
